import { render } from 'solid-js/web';

import { App } from './app';

describe('<App />', () => {
  it('renders without errors', () => {
    const div = document.createElement('div');
    expect(() => {
      render(() => <App />, div);
    }).not.toThrow();
  });
});
