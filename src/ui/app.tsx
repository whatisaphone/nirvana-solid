import { JSX } from 'solid-js';

import classes from './app.module.scss';

export function App(): JSX.Element {
  return <h1 class={classes.app}>nirvana-solid</h1>;
}
