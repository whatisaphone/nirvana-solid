import { render } from 'solid-js/web';

import { App } from './ui/app';

main();

function main() {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const mount = document.getElementById('app')!;
  render(() => <App />, mount);
}
